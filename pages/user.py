import yaml
import os

present_dir = os.path.dirname(os.path.abspath(__file__))
path_to_yaml = os.path.join(present_dir, r'..\config.yaml')

with open(path_to_yaml, "r", encoding='utf-8') as f:
    data = yaml.safe_load(f)


class User:

    @staticmethod
    def get_user_email():
        return data["litecart"]["user"]["username"]

    @staticmethod
    def get_user_password():
        return data["litecart"]["user"]["password"]
