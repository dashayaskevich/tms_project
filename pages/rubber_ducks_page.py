from pages.base_page import BasePage
from pages.locators.ducks_category_page_locators\
    import DucksCategoryLocators
from pages.product_detail_page import ProductDetailPage


class RubberDucksPage(BasePage):

    def __init__(self, driver, url):
        self.url = url
        super().__init__(driver, self.url)

    def get_all_products(self):
        all_items = self.driver.find_elements(
            *DucksCategoryLocators.ALL_ITEMS_ON_PAGE)
        return all_items

    def open_product_detail_page(self, title):
        products = self.get_all_products()
        item = next(filter(
            lambda item: item.get_attribute('title') == title, products), None)
        item.click()
        return ProductDetailPage(self.driver, self.driver.current_url)
