from selenium.webdriver import Keys
from selenium.webdriver.support.ui import Select
from pages.base_page import BasePage
from pages.locators.regional_settings_page_locators import\
    RegionalSettingsPageLocators


class RegionalSettingsPage(BasePage):

    def set_currency(self, currency_code):
        """
        Select currency_code from a dropdown list.
        """
        drp = Select(self.driver.find_element(
            *RegionalSettingsPageLocators.CURRENCY_DROPDOWN_BOX))
        drp.select_by_value(currency_code)

    def set_country(self, country_name):
        self.click(RegionalSettingsPageLocators.COUNTRY_DROPDOWN_BOX)
        search_field = self.driver.find_element(
            *RegionalSettingsPageLocators.COUNTRY_SEARCH_FIELD)
        search_field.send_keys(country_name)
        search_field.send_keys(Keys.RETURN)

    def submit_changes(self):
        self.click(RegionalSettingsPageLocators.SAVE_BUTTON)
