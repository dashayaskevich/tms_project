from pages.edit_account_page import EditAccountPage
from pages.base_page import BasePage
from pages.header import Header
from pages.footer import Footer
from pages.locators.main_page_locators import\
    LoginFormLocators, CategoriesLinks, AccountLinks, MainPageLocators
from pages.rubber_ducks_page import RubberDucksPage


class MainPage(BasePage):

    def __init__(self, driver, url):
        self.url = url
        self.header = Header(driver, self.url)
        self.footer = Footer(driver, self.url)
        super().__init__(driver, self.url)

    def login(self, email, password):
        self.driver.find_element(
            *LoginFormLocators.EMAIL_FIELD).send_keys(email)
        self.driver.find_element(
            *LoginFormLocators.PASSWORD_FIELD).send_keys(password)
        self.click(LoginFormLocators.LOGIN_BUTTON)

    def logout(self):
        self.click(MainPageLocators.LOGOUT_LINK)

    def open_rubber_ducks_category_page(self):
        self.click(CategoriesLinks.RUBBER_DUCKS_LINK)
        return RubberDucksPage(self.driver, self.driver.current_url)

    def open_edit_account_page(self):
        self.click(AccountLinks.EDIT_ACCOUNT_LINK)
        return EditAccountPage(self.driver, self.driver.current_url)
