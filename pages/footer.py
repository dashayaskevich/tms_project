from pages.base_page import BasePage
from pages.locators.footer_locators import FooterLinksLocators
from pages.regional_settings_page import RegionalSettingsPage


class Footer(BasePage):
    def open_regional_settings_page(self):
        self.click(FooterLinksLocators.REGIONAL_SETTINGS_LINK)
        return RegionalSettingsPage(self.driver, self.driver.current_url)
