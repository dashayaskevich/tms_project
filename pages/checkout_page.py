from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.base_page import BasePage
from pages.locators.checkout_page_locators\
    import ItemsInCartLocators, OrderSummary


class CheckoutPage(BasePage):

    def change_quantity(self, number):
        self.driver.find_element(*ItemsInCartLocators.QUANTITY).clear()
        self.driver.find_element(
            *ItemsInCartLocators.QUANTITY).send_keys(str(number))
        self.click(ItemsInCartLocators.UPDATE_BUTTON)
        WebDriverWait(self.driver, timeout=10).until(
            EC.text_to_be_present_in_element(
                OrderSummary.QUANTITY, str(number)))

    def get_item_price(self):
        return self.get_element_text(ItemsInCartLocators.ITEM_PRICE)

    def get_item_title(self):
        return self.get_element_text(ItemsInCartLocators.ITEM_TITLE)

    def get_item_size(self):
        return self.get_element_text(ItemsInCartLocators.SIZE)

    def get_item_quantity_in_input_box(self):
        quantity_element = self.driver.find_element(
            *ItemsInCartLocators.QUANTITY)
        return quantity_element.get_attribute("value")

    def remove_item(self):
        self.click(ItemsInCartLocators.REMOVE_BUTTON)
        WebDriverWait(self.driver, timeout=10).until(
            EC.visibility_of_element_located(
                ItemsInCartLocators.EMPTY_CART_MESSAGE))

    def return_from_empty_cart(self):
        self.click(ItemsInCartLocators.BACK_LINK)

    def get_empty_cart_message(self):
        return self.get_element_text(ItemsInCartLocators.EMPTY_CART_MESSAGE)

    def get_payment_due(self):
        return self.get_element_text(OrderSummary.PAYMENT_DUE)

    def get_item_title_in_order_summary(self):
        return self.get_element_text(OrderSummary.ITEM_TITLE)

    def get_item_quantity_in_order_summary(self):
        return self.get_element_text(OrderSummary.QUANTITY)

    def confirm_order(self):
        self.click(OrderSummary.CONFIRM_ORDER_BUTTON)
        WebDriverWait(self.driver, timeout=10).until(
            EC.title_contains("Order Success"))

    def get_all_items(self):
        return self.driver.find_elements(*ItemsInCartLocators.ALL_ITEMS)
