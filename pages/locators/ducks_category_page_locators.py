from selenium.webdriver.common.by import By


class DucksCategoryLocators:
    ALL_ITEMS_ON_PAGE = (By.CSS_SELECTOR,
                         'ul.listing-wrapper.products li a.link')
