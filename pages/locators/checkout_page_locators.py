from selenium.webdriver.common.by import By


class ItemsInCartLocators:
    ITEM_PRICE = (By.CSS_SELECTOR, 'li.item div > p:nth-child(2)')
    QUANTITY = (By.CSS_SELECTOR, 'input[name="quantity"]')
    UPDATE_BUTTON = (By.XPATH, '//button[text()="Update"]')
    REMOVE_BUTTON = (By.XPATH, '//button[text()="Remove"]')
    ITEM_TITLE = (By.CSS_SELECTOR, 'li.item p strong')
    SIZE = (By.CSS_SELECTOR, 'li.item p:nth-child(2)')

    ALL_ITEMS = (By.CSS_SELECTOR, 'ul.items > li')

    EMPTY_CART_MESSAGE = (By.CSS_SELECTOR, '#checkout-cart-wrapper em')
    BACK_LINK = (By.CSS_SELECTOR, '#checkout-cart-wrapper a')


class OrderSummary:
    QUANTITY = (By.XPATH,
                '//tr[@class="header"]/following-sibling::tr[1]/*[1]')
    ITEM_TITLE = (By.CSS_SELECTOR, 'td.item')
    TOTAL = (By.CSS_SELECTOR, 'td.sum')
    PAYMENT_DUE = (By.CSS_SELECTOR, 'tr.footer > td:nth-child(2) > strong')
    CONFIRM_ORDER_BUTTON = (By.XPATH, '//button[text()="Confirm Order"]')
