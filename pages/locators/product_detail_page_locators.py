from selenium.webdriver.common.by import By


class ProductPageLocators:
    SIZE_DROPDOWN_BOX = (By.CSS_SELECTOR, 'td.options select')
    QUANTITY = (By.CSS_SELECTOR, 'input[name="quantity"]')
    PRODUCT_PRICE = (By.CSS_SELECTOR, 'span.price')
    ADD_TO_CART_BUTTON = (By.CSS_SELECTOR, 'button[type="submit"]')
