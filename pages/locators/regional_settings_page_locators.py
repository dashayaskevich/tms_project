from selenium.webdriver.common.by import By


class RegionalSettingsPageLocators:
    COUNTRY_DROPDOWN_BOX = (
        By.XPATH,
        '//td[text() = "Country"]//span[@class="select2-selection__rendered"]')
    COUNTRY_SEARCH_FIELD = (By.CSS_SELECTOR, 'input.select2-search__field')
    CURRENCY_DROPDOWN_BOX = (By.XPATH, '//select[@name="currency_code"]')
    SAVE_BUTTON = (By.CSS_SELECTOR, 'button[type="submit"]')
