from selenium.webdriver.common.by import By


class MainPageLocators:
    REGIONAL_SETTINGS_LINK = (By.LINK_TEXT, 'Regional Settings')
    COUNTRY = (By.CLASS_NAME, 'country')
    CURRENCY = (By.CSS_SELECTOR, '.currency > span')
    LOGOUT_LINK = (By.LINK_TEXT, "Logout")
    NOTIFICATION_LOCATOR = (By.CSS_SELECTOR, '.notice.success')


class LoginFormLocators:
    EMAIL_FIELD = (By.XPATH, '//input[@name="email"]')
    PASSWORD_FIELD = (By.XPATH, '//input[@name="password"]')
    LOGIN_BUTTON = (By.XPATH, '//button[@name="login"]')


class CategoriesLinks:
    RUBBER_DUCKS_LINK = (By.LINK_TEXT, 'Rubber Ducks')


class AccountLinks:
    EDIT_ACCOUNT_LINK = (By.LINK_TEXT, 'Edit Account')
