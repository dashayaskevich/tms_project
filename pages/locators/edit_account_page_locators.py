from selenium.webdriver.common.by import By


class EditAccountLocators:
    FIRST_NAME_FIELD = (By.CSS_SELECTOR, 'input[name="firstname"]')
    LAST_NAME_FIELD = (By.CSS_SELECTOR, 'input[name="lastname"]')

    SAVE_BUTTON = (By.CSS_SELECTOR, 'button[name="save"]')
