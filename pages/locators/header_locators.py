from selenium.webdriver.common.by import By


class MiniCartLocators:
    QUANTITY = (By.CSS_SELECTOR, '#cart span.quantity')
    PRICE = (By.CSS_SELECTOR, '#cart span.formatted_value')
    CHECKOUT_LINK = (By.PARTIAL_LINK_TEXT, 'Checkout')


class RegionSettingsLocators:
    COUNTRY = (By.CLASS_NAME, 'country')
    CURRENCY = (By.CSS_SELECTOR, '.currency > span')
