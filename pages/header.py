from pages.base_page import BasePage
from pages.checkout_page import CheckoutPage
from pages.locators.header_locators import MiniCartLocators,\
    RegionSettingsLocators


class Header(BasePage):

    def get_selected_currency(self):
        """
        :return: currently chosen currency code
        """
        return self.get_element_text(RegionSettingsLocators.CURRENCY)

    def get_selected_country(self):
        """
        :return: currently chosen country name
        """
        return self.get_element_text(RegionSettingsLocators.COUNTRY)

    def open_checkout_page(self):
        self.click(MiniCartLocators.CHECKOUT_LINK)
        return CheckoutPage(self.driver, self.driver.current_url)

    def get_quantity_in_mini_cart(self):
        return self.get_element_text(MiniCartLocators.QUANTITY)

    def get_price_in_mini_cart(self):
        return self.get_element_text(MiniCartLocators.PRICE)
