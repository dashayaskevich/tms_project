from pages.base_page import BasePage
from pages.locators.edit_account_page_locators import EditAccountLocators
from selenium.webdriver import Keys


class EditAccountPage(BasePage):

    def change_first_name(self, new_name):
        field = self.driver.find_element(*EditAccountLocators.FIRST_NAME_FIELD)
        field.clear()
        field.send_keys(new_name)
        field.send_keys(Keys.RETURN)

    def submit_changes(self):
        self.click(EditAccountLocators.SAVE_BUTTON)
