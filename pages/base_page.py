from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:

    def __init__(self, driver, url):
        self.driver = driver
        self.url = url

    def open_page(self):
        self.driver.get(self.url)

    def find_element(self, locator, timeout=10):
        return WebDriverWait(self.driver, timeout).until(
            EC.presence_of_element_located(locator)
        )

    def get_page_title(self):
        return self.driver.title

    def click(self, locator):
        self.driver.find_element(*locator).click()

    def get_element_text(self, locator):
        return self.driver.find_element(*locator).text
