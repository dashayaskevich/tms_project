from pages.base_page import BasePage
from pages.locators.product_detail_page_locators\
    import ProductPageLocators
from pages.header import Header
from pages.locators.header_locators import MiniCartLocators
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class ProductDetailPage(BasePage):

    def __init__(self, driver, url):
        self.header = Header(driver, url)
        super().__init__(driver, url)

    def set_quantity(self, number):
        self.driver.find_element(*ProductPageLocators.QUANTITY).clear()
        self.driver.find_element(
            *ProductPageLocators.QUANTITY).send_keys(str(number))

    def set_size(self, size_value):
        """
        Select item size from a dropdown list.
        Possible size values: Small, Medium, Large.
        """
        drp = Select(self.driver.find_element(
            *ProductPageLocators.SIZE_DROPDOWN_BOX))
        drp.select_by_value(size_value.capitalize())

    def add_product_to_cart(self, quantity):
        """
        Add to cart item(s) with no size options.
        """
        self.set_quantity(quantity)
        self.click(ProductPageLocators.ADD_TO_CART_BUTTON)
        WebDriverWait(self.driver, timeout=10).until(
            EC.text_to_be_present_in_element(
                MiniCartLocators.QUANTITY, str(quantity)))

    def add_to_cart_product_with_size_option(self, size, quantity):
        """
        Add to cart item(s) with size options.
        """
        self.set_quantity(quantity)
        self.set_size(size)
        self.click(ProductPageLocators.ADD_TO_CART_BUTTON)
        WebDriverWait(self.driver, timeout=10).until(
            EC.text_to_be_present_in_element(MiniCartLocators.QUANTITY,
                                             str(quantity)))

    def get_product_price(self):
        return self.get_element_text(ProductPageLocators.PRODUCT_PRICE)
