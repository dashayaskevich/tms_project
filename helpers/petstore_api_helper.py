import requests


class Endpoints:
    PET = "/pet"
    PET_ID = "/pet/{id}"
    USER = "/user"
    USER_NAME = "/user/{username}"
    USER_LOGIN = "/user/login"


def check_response(response):
    response.raise_for_status()
    return response


class RestHelper:
    def __init__(self, url, api_key=None):
        self.url = url
        self.api_key = api_key
        self.session = requests.Session()
        if self.api_key:
            self.session.headers.update({"X-API-Key": api_key})

    def do_get(self, endpoint, **kwargs):
        response = self.session.get(self.url + endpoint, **kwargs)
        return check_response(response).json()

    def do_post(self, endpoint, **kwargs):
        response = self.session.post(self.url + endpoint, **kwargs)
        return check_response(response).json()

    def do_put(self, endpoint, **kwargs):
        response = self.session.put(self.url + endpoint, **kwargs)
        return check_response(response).json()

    def do_delete(self, endpoint):
        response = self.session.delete(self.url + endpoint)
        return check_response(response)


class PetstoreApiHelper(RestHelper):

    def __init__(self, base_url, api_key, api_version):
        self.api_key = api_key
        self.api_version = api_version
        self.url = f"{base_url}{self.api_version}"
        super().__init__(self.url, api_key)

    def get_pet_by_id(self, pet_id: int):
        return self.do_get(Endpoints.PET_ID.format(id=pet_id))

    def add_pet(self, pet_id: int, pet_name: str, status):
        data = {
            "id": pet_id,
            "category": {
                "id": pet_id,
                "name": pet_name
            },
            "name": pet_name,
            "photoUrls": [
                "string"
            ],
            "tags": [
                {
                    "id": pet_id,
                    "name": pet_name
                },

            ],
            "status": status
        }
        return self.do_post(Endpoints.PET, json=data)

    def delete_pet(self, pet_id: int):
        return self.do_delete(Endpoints.PET_ID.format(id=pet_id))

    def add_user(self,
                 user_id: int,
                 username, first_name, last_name, email, password, phone,
                 user_status: int):
        data = {
            "id": user_id,
            "username": username,
            "firstName": first_name,
            "lastName": last_name,
            "email": email,
            "password": password,
            "phone": phone,
            "userStatus": user_status
        }
        return self.do_post(Endpoints.USER, json=data)

    def get_user_by_username(self, username):
        return self.do_get(Endpoints.USER_NAME.format(username=username))

    def login_user(self, username, password):
        params = {
            "username": username,
            "password": password
        }
        return self.do_get(Endpoints.USER_LOGIN, params=params)

    def change_user_data(self, username,
                         user_id: int,
                         new_username,
                         first_name,
                         last_name,
                         email,
                         password,
                         phone,
                         user_status: int):
        data = {
            "id": user_id,
            "username": new_username,
            "firstName": first_name,
            "lastName": last_name,
            "email": email,
            "password": password,
            "phone": phone,
            "userStatus": user_status
        }
        return self.do_put(Endpoints.USER_NAME.format(
            username=username), json=data)
