import mysql.connector as mysql


class DatabaseHelper:

    def __init__(self, host, user, database, password):
        self.host = host
        self.user = user
        self.database = database
        self.password = password
        self._connection = mysql.connect(
            host=self.host,
            user=self.user,
            database=self.database,
            password=self.password
        )
        self._cursor = self._connection.cursor()

    @property
    def connection(self):
        return self._connection

    @property
    def cursor(self):
        return self._cursor

    def query_executor(self, query, *args, return_result=False, commit=False,
                       many=False):
        result = None
        if many:
            self.cursor.executemany(query, *args)
        else:
            self.cursor.execute(query, *args)

        if return_result:
            result = self.cursor.fetchall()
        if commit:
            result = self.connection.commit()
        return result

    def close_connection(self):
        self.connection.close()


class MySQLHelper(DatabaseHelper):

    def get_orders_by_customer_email(self, customer_email):
        query = f"SELECT * FROM lc_orders WHERE" \
                f" customer_email = '{customer_email}'"
        return self.query_executor(query, return_result=True)

    def get_customer_first_name(self, customer_email):
        query = f"SELECT firstname FROM lc_customers WHERE" \
                f" email = '{customer_email}'"
        return self.query_executor(query, return_result=True)[0][0]

    def delete_user_order(self, customer_email):
        query = f"DELETE FROM lc_orders WHERE" \
                f" customer_email = '{customer_email}'"
        self.query_executor(query, commit=True)

    def empty_user_cart(self, customer_id):
        query = f"DELETE FROM lc_cart_items WHERE" \
                f" customer_id = '{customer_id}'"
        self.query_executor(query, commit=True)

    def get_customer_id(self, customer_email):
        query = f"SELECT id FROM lc_customers WHERE" \
                f" email = '{customer_email}'"
        return self.query_executor(query, return_result=True)[0][0]

    def get_cart_items(self, customer_id):
        query = f"SELECT * FROM lc_cart_items WHERE" \
                f" customer_id = '{customer_id}'"
        return self.query_executor(query, return_result=True)

    def change_user_first_name(self, customer_email, firstname):
        query = f"UPDATE lc_customers SET firstname = '{firstname}' WHERE" \
                f" email = '{customer_email}'"
        self.query_executor(query, commit=True)
