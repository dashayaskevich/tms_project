from pages.user import User
import allure


@allure.story("Check user can change his first name")
def test_edit_account_change_first_name(login_user, db):
    with allure.step('Get first name of the customer from database by email'):
        customer_email = User.get_user_email()
        original_first_name = db.get_customer_first_name(customer_email)
    with allure.step('Open edit account page as a default user'):
        edit_account_page = login_user.open_edit_account_page()
    with allure.step('Set a new First Name'):
        new_name = 'New Name'
        edit_account_page.change_first_name(new_name)
        edit_account_page.submit_changes()
    with allure.step('Verify that firstname is changed in database'):
        user_name = db.get_customer_first_name(customer_email)
        assert new_name == user_name, "First name is not correct"
    with allure.step('Set original first name value'):
        db.change_user_first_name(customer_email, original_first_name)
