from pages.user import User
import allure


@allure.story("Check mini cart displays the correct price")
def test_mini_cart_price(login_user, db, db_cart_cleaner):
    with allure.step('Add three ducks to cart'):
        product_page = __add_to_cart('Red Duck', 3, login_user)

    assert product_page.header.get_price_in_mini_cart() == '$60',\
        "Incorrect price in mini cart"


@allure.story("Check mini cart displays the correct quantity")
def test_mini_cart_quantity(login_user, db, db_cart_cleaner):
    with allure.step('Add three ducks to cart'):
        product_page = __add_to_cart('Red Duck', 3, login_user)

    assert product_page.header.get_quantity_in_mini_cart() == '3',\
        "Incorrect quantity in mini cart"


@allure.story("Check correct item size is displayed on the checkout page")
def test_checkout_item_size(login_user, db, db_order_cleaner,
                            db_cart_cleaner):
    with allure.step('Open Rubber Ducks category page'):
        category_page = login_user.open_rubber_ducks_category_page()
    with allure.step('Open a product detail page with a yellow duck'):
        product_page = category_page.open_product_detail_page('Yellow Duck')
    with allure.step('Add three ducks to cart'):
        product_page.add_to_cart_product_with_size_option(size='Small',
                                                          quantity=3)
    with allure.step('Go to the checkout page'):
        checkout_page = product_page.header.open_checkout_page()

    assert 'Small' in checkout_page.get_item_size(), "Incorrect size value"


@allure.story("Check correct item title is displayed on the checkout page")
def test_checkout_item_title(login_user, db, db_cart_cleaner):
    with allure.step('Add three ducks to cart'):
        product_title = 'Red Duck'
        product_page = __add_to_cart(product_title, 3,
                                     login_user)
    with allure.step('Open the checkout page'):
        checkout_page = product_page.header.open_checkout_page()

    assert checkout_page.get_item_title() == product_title,\
        "Incorrect item title"


@allure.story("Check the price in the payment due is correct")
def test_checkout_payment_due(login_user, db, db_cart_cleaner):
    with allure.step('Add three ducks to cart'):
        product_page = __add_to_cart('Red Duck', 3, login_user)
    with allure.step('Open the checkout page'):
        checkout_page = product_page.header.open_checkout_page()

    assert checkout_page.get_payment_due() == '$60.00',\
        "Incorrect payment due value"


@allure.story("Change item quantity on the checkout page")
def test_checkout_change_quantity_quantity_in_input_box(
        login_user, db, db_cart_cleaner):
    with allure.step('Add one duck to cart'):
        product_page = __add_to_cart('Red Duck', 1, login_user)
    with allure.step('Open the checkout page'):
        checkout_page = product_page.header.open_checkout_page()
    with allure.step('Change item quantity to 3'):
        checkout_page.change_quantity(3)

    assert checkout_page.get_item_quantity_in_input_box() == '3',\
        "Incorrect item quantity"


@allure.story("Change item quantity on the checkout page")
def test_checkout_change_quantity_order_summary_quantity(
        login_user, db, db_cart_cleaner):
    with allure.step('Add one duck to cart'):
        product_page = __add_to_cart('Red Duck', 1, login_user)
    with allure.step('Open the checkout page'):
        checkout_page = product_page.header.open_checkout_page()
    with allure.step('Change item quantity to 3'):
        checkout_page.change_quantity(3)

    assert checkout_page.get_item_quantity_in_order_summary() == '3',\
        "Incorrect item quantity in order summary"


@allure.story("Change item quantity on the checkout page")
def test_checkout_change_quantity_payment_due(login_user,
                                              db, db_cart_cleaner):
    with allure.step('Add one duck to cart'):
        product_page = __add_to_cart('Red Duck', 1, login_user)
    with allure.step('Open the checkout page'):
        checkout_page = product_page.header.open_checkout_page()
    with allure.step('Change item quantity to 3'):
        checkout_page.change_quantity(3)

    assert checkout_page.get_payment_due() == '$60.00',\
        "Incorrect payment due value"


@allure.story("Remove items from the cart")
def test_clear_cart_ui(login_user, db, db_cart_cleaner):
    with allure.step('Add three ducks to cart'):
        product_page = __add_to_cart('Red Duck', 3, login_user)
    with allure.step('Open the checkout page'):
        checkout_page = product_page.header.open_checkout_page()
    with allure.step('Remove items from cart'):
        checkout_page.remove_item()

    assert "There are no items in your cart." in \
           checkout_page.get_empty_cart_message(), "Cart is not empty"


@allure.story("Remove user's cart items from database")
def test_clear_cart_db(login_user, db, db_cart_cleaner):
    with allure.step('Add three ducks to cart'):
        product_page = __add_to_cart('Red Duck', 3, login_user)
    with allure.step('Open the checkout page'):
        checkout_page = product_page.header.open_checkout_page()
    with allure.step('Remove items from cart'):
        checkout_page.remove_item()
    with allure.step('Get customer id from database by user email'):
        customer_id = db.get_customer_id(User.get_user_email())
    with allure.step('Check the user with the following id has no cart items'
                     ' in database'):
        assert db.get_cart_items(customer_id) == [],\
            "User cart items are not erased"


@allure.story("Check that created order appears in the database")
def test_create_order_db(login_user, db, db_order_cleaner,
                         db_cart_cleaner):
    with allure.step('Add three ducks to cart'):
        product_page = __add_to_cart('Red Duck', 3, login_user)
    with allure.step('Open the checkout page'):
        checkout_page = product_page.header.open_checkout_page()
    with allure.step('Confirm the order'):
        checkout_page.confirm_order()
    with allure.step('Check the order has appeared in the database'):
        assert db.get_orders_by_customer_email(
            User.get_user_email()) != [], "Created order is not found"


def __add_to_cart(product_title, quantity, page):
    """
    This method is for selecting item by product_title page
    and adding the item to cart.
    """
    category_page = page.open_rubber_ducks_category_page()
    product_page = category_page.open_product_detail_page(product_title)
    product_page.add_product_to_cart(quantity)
    return product_page
