import pytest
import yaml
from selenium import webdriver
from helpers.mysql_helper import MySQLHelper
from pages.main_page import MainPage
from pages.user import User
import os

present_dir = os.path.dirname(os.path.abspath(__file__))
path_to_yaml = os.path.join(present_dir, r'..\..\config.yaml')

with open(path_to_yaml, "r", encoding='utf-8') as f:
    data = yaml.safe_load(f)

HOST = data["mysql"]["host"]
USER = data["mysql"]["user"]
PASSWD = data["mysql"]["password"]
DATABASE = data["mysql"]["db"]

BASE_URL = data["litecart"]["urls"]["base_url"]

path_to_chromedriver = data["webdriver_path"]


@pytest.fixture(scope="session")
def driver():
    driver = webdriver.Chrome(path_to_chromedriver)
    yield driver
    driver.quit()


@pytest.fixture
def open_main_page(driver):
    page = MainPage(driver, BASE_URL)
    page.open_page()
    return page


@pytest.fixture(scope="function")
def login_user(driver):
    username = User.get_user_email()
    password = User.get_user_password()
    page = MainPage(driver, BASE_URL)
    page.open_page()
    page.login(username, password)
    yield page
    page = MainPage(driver, BASE_URL)
    page.open_page()
    page.logout()


@pytest.fixture(scope='session')
def db():
    db = MySQLHelper(HOST, USER, DATABASE, PASSWD)
    yield db
    db.close_connection()


@pytest.fixture(scope='function')
def db_order_cleaner(db):
    """
    Delete order made by a user from database after the test execution.
    """
    yield
    customer_email = User.get_user_email()
    db.delete_user_order(customer_email)


@pytest.fixture(scope='function')
def db_cart_cleaner(db):
    """
    Delete items in a user's cart from database after the test execution.
    """
    yield
    customer_email = User.get_user_email()
    customer_id = db.get_customer_id(customer_email)
    db.empty_user_cart(customer_id)
