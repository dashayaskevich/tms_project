import allure


@allure.story("Change currency through the regional settings page")
def test_regional_settings_change_currency(open_main_page):
    with allure.step('Open the main page'):
        page = open_main_page
    with allure.step('Get selected currency code from the main page'):
        currency_choice = None
        selected_currency = page.header.get_selected_currency()
        if selected_currency == 'USD':
            currency_choice = 'EUR'
        elif selected_currency == 'EUR':
            currency_choice = 'USD'
    with allure.step('Go to the regional settings page'):
        settings_page = page.footer.open_regional_settings_page()
    with allure.step('Select the chosen currency code from dropdown list'):
        settings_page.set_currency(currency_choice)
        settings_page.submit_changes()

    assert page.header.get_selected_currency() == currency_choice,\
        "Incorrect currency code is displayed"


@allure.story("Change country through the regional settings page")
def test_regional_settings_change_country(open_main_page):
    with allure.step('Open the main page'):
        page = open_main_page
    with allure.step('Open the regional settings page'):
        settings_page = page.footer.open_regional_settings_page()
    with allure.step('Enter a chosen country name into the "Country" field,'
                     ' submit changes'):
        country_choice = 'Ireland'
        settings_page.set_country(country_choice)
        settings_page.submit_changes()

    assert page.header.get_selected_country() == country_choice,\
        "Incorrect country is displayed"
