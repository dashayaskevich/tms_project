import pytest
from requests.exceptions import HTTPError
import allure


@allure.story("Check new pet can be created")
def test_add_pet(api_client):
    with allure.step('Create a new pet'):
        new_pet_id = 111
        api_client.add_pet(new_pet_id, "Dog Name", "available")
    with allure.step('Check the new pet exists'):
        new_pet = api_client.get_pet_by_id(new_pet_id)

    assert new_pet, "Created pet is not found"


@allure.story("Delete a pet")
def test_delete_pet(api_client):
    with allure.step('Create a new pet'):
        new_pet_id = 111
        api_client.add_pet(new_pet_id, "Dog Name", "available")
    with allure.step('Delete the created pet by id'):
        api_client.delete_pet(new_pet_id)
    with allure.step('Check the created pet does not exist'):
        with pytest.raises(HTTPError):
            api_client.get_pet_by_id(new_pet_id)


@allure.story("Create a new user")
def test_add_user(api_client):
    with allure.step('Create a new user'):
        username = 'tester_user'
        api_client.add_user(0,
                            username,
                            'Tester Name',
                            'Tester Surname',
                            'tester_email@gmail.com',
                            'tester_password',
                            '01632960964',
                            0)
    with allure.step('Get the created user by username'):
        new_user = api_client.get_user_by_username(username)

    assert new_user, "Created user is not found"


@allure.story("Updating the username of a user")
def test_change_username(api_client):
    with allure.step('Create a new user'):
        username = 'tester_user'
        password = 'tester_password'
        api_client.add_user(0,
                            username,
                            'Tester Name',
                            'Tester Surname',
                            'tester_email@gmail.com',
                            password,
                            '01632960964',
                            0)
    with allure.step('Log in as the created user'):
        api_client.login_user(username, password)
    with allure.step('Change the username to a new one'):
        new_username = 'tester_user_new'
        response = api_client.change_user_data(username,
                                               0,
                                               new_username,
                                               'Tester Name',
                                               'Tester Surname',
                                               'tester_email@gmail.com',
                                               'tester_password',
                                               '01632960964',
                                               0)
    assert response['code'] == 200, "User data has not been updated"
