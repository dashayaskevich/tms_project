import pytest
import yaml
from helpers.petstore_api_helper import PetstoreApiHelper
import os

present_dir = os.path.dirname(os.path.abspath(__file__))
path_to_yaml = os.path.join(present_dir, r'..\..\config.yaml')

with open(path_to_yaml, "r", encoding='utf-8') as f:
    data = yaml.safe_load(f)

API_KEY = data["petstore"]["api_key"]
API_VERSION = data["petstore"]["api_version"]
BASE_URL = data["petstore"]["base_url"]


@pytest.fixture(scope='session')
def api_client():
    api_client = PetstoreApiHelper(BASE_URL, API_KEY, API_VERSION)
    return api_client
